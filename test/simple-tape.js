const test = require('blue-tape');
const redis = require('redis');

const REDIS_TIMEOUT_MS = 1000;

function createTestObj(arg) {
  return new Promise((resolve) => {
    resolve(`bam${arg + 1}`);
  });
}

function connectRedis(arg) {
  return new Promise((resolve) => {
    const client = redis.createClient(arg);
    resolve(client);
  });
}

// Test out node promise syntax:
test('TestPromises', (t) => {
  createTestObj(2112)
    .then((msg) => {
      console.log('Test Obj', msg);
      t.equals(msg, 'bam2113', 'Return messsage should increment and prepend "bam".');
      t.pass('Promises Good...');
      t.end();
    }).catch(err => t.fail('Error while testing', err));
});

test('TestRedis', (t) => {
  connectRedis()
    .then((client) => {
      t.ok(client, 'Connect returns a client.');

      // Quit client after a pause:
      setTimeout(() => {
        client.quit();
        t.pass('Redis Good...');
        t.end();
      }, REDIS_TIMEOUT_MS);

      // Time the test out if we don't shut down normally:
      t.timeoutAfter(REDIS_TIMEOUT_MS * 1.5);

      // We should have plenty of time to do this:
      client.set('foo', 'bar2112', redis.print);
      client.get('foo', (err, reply) => {
        console.log('FOOOOO:', reply);
        t.equals(reply, 'bar2112', 'Redis should set and get.');
      });

      client.hmset('fam', 'dad', 'kid1');
      client.hmset('fam', 'mom', 'kid2');
      client.hmset('fam', 'uxy', 'kid3,kid4');

      // Get all of fam:
      client.hgetall('fam', (err, reply) => {
        t.equal(typeof reply, 'object');
        t.equal(reply.dad, 'kid1');
        t.equal(reply.mom, 'kid2');
        t.equal(reply.uxy, 'kid3,kid4'); // Note, not an array
      });

      // Get mom:
      client.hget(['fam', 'mom'], (err, reply) => {
        t.equal(reply, 'kid2');
      });
    }).catch(err => t.fail('Error while testing', err));
});

test('TestRedisPubSub', (t) => {
  const subscriber = redis.createClient();
  const publisher = redis.createClient();
  let gimli = false;
  let legolas = false;

  subscriber.on('message', (channel, message) => {
    console.log(`Message '${message}' on channel '${channel}' arrived!`);
    if (message === 'gimli') {
      gimli = true;
    } else if (message === 'legolas') {
      legolas = true;
    }

    if (gimli && legolas) {
      t.pass('PubSub Good...');
      subscriber.quit();
      publisher.quit();
      t.end();
    }
  });

  subscriber.subscribe('fooChannel');

  t.timeoutAfter(REDIS_TIMEOUT_MS);
  publisher.publish('fooChannel', 'gimli');
  publisher.publish('fooChannel', 'legolas');
});
