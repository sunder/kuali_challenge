const test = require('blue-tape');
const ElevatorSystem = require('../src/elevator-system');


test('TestExistence', (t) => {
  t.ok(ElevatorSystem);
  t.throws(() => {
    let system = ElevatorSystem();
  }, Error, 'Should supply reasonable number of floors.');
  t.pass('Good ElevatorSystem...');
  t.end();
});


test('Sensible numbers', (t) => {
  t.throws(() => {
    let system = ElevatorSystem(2, 1);
  }, Error, 'Should have a reasonable number of floors.');

  t.throws(() => {
    let system = ElevatorSystem(2, 0);
  }, Error, 'Should have a reasonable number of floors.');

  t.throws(() => {
    let system = ElevatorSystem(0, 2);
  }, Error, 'Should have a reasonable number of elevators.');

  t.end();
});

test('Handles Floor changes', (t) => {
  const elevatorCount = 99;
  const floorCount = 111;

  let system = ElevatorSystem(elevatorCount, floorCount);
  let elevator = system.getElevator(elevatorCount-1)

  t.throws(() => {
    system.getElevator(elevatorCount)
  }, Error, 'Too high of an elevator numer.');

  t.throws(() => {
    system.elevatorMoved(elevatorCount, 44, 'up');
  }, Error, 'Too high of an elevator numer.');

  // TODO: Maybe make method this return a promise?
  system.elevatorMoved(8, 44, 'up');
  elevator = system.getElevator(8);

  console.log('This is the elevator 8', elevator);
  t.ok(elevator);
  t.equals(parseInt(elevator.currentFloor), 44, 'Should change floor');
  t.equals(parseInt(elevator.usage), 44, 'Should change usage');
  t.equals(parseInt(elevator.trips), 1, 'Should change trips');

  // Move down and then check again:
  system.elevatorMoved(8, 11, 'down');

  elevator = system.getElevator(8);
  t.equals(parseInt(elevator.currentFloor), 11, 'Should change floor');
  t.equals(parseInt(elevator.usage), 77, 'Should change usage');
  t.equals(parseInt(elevator.trips), 2, 'Should change trips');

  t.pass('ElevatorSystem Good...');
  t.end();
});
