## Node app for Kuali take-home test.  Requires a Redis server to be running on default port for tests.

# Currently, there are two unit test files:
* simple-tape - verify some basic promise functionality and redis
* elevator-tape - verify that we can initialize and update elevator in system

## On Mac:
brew install redis
brew services start redis

# Change to app directory:
cd ~/src/kuali/take-home
npm install
npm test


# Notes:
* Is not fully functional.  The current tests pass and provides a starting point for a discussion.
