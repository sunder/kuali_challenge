const cuid = require('cuid');

/** Initialize elevator system with number of elevators and number of floors.
    Stores in Redis so this module may be used as part of a microservice architecture,
    where multiple server resources perform work, and elevators are communicated with

    Future versions may allow more granular settings for things like:
    . specifying "banks" of elevators that allow grouping based on location or call button grouping
    . Express elevators that skip certain floors
    . Specify behavior for emergency conditions (door open or closed, try to stop on certain floor, etc:)
    . Initialize with existing elevators that communicate their own status:
*/
function ElevatorSystem(elevatorCount, floorCount) {
  console.log("Creating elevator system", !!elevatorCount)
  if (!elevatorCount || elevatorCount < 1) {
    throw new Error('System requires at least one elevator.');
  } else if (!floorCount || floorCount === 1) {
    throw new Error('You do not need an elevator.');
  } else if (floorCount <= 0) {
    throw new Error('You really do not need an elevator.');
  }

  // Elevator and floor count are 0-based.  Add 1 in UI when reporting to user.
  let config = {
    elevatorCount: elevatorCount,
    floorCount: floorCount,
  };

  let state = {
    elevators: [],
    floors:[],
  };

  for (let i = 0; i < elevatorCount; ++i) {
    state.elevators[i] = {
      uid: cuid(),
      index: i,
      currentFloor: 0,
      usage: 0,
      trips: 0,
      doorOpen: false,
      goals: null,
    };
  }

  for (let i = 0; i < floorCount; ++i) {
    state.floors[i] = {
      uid: cuid(),
      index: i,
      callButtonUpLit: false,
      callButtonDownLit: false,
    };
  }

  return {
    getFloorCount: ()=> {
        return floorCount;
    },

    getElevatorCount: ()=> {
        return elevatorCount;
    },

    getElevator: (elevatorIndex)=> {
      // TODO: Also check against array length:
      if (elevatorIndex < 0 || elevatorIndex >= config.elevatorCount) {
        throw new Error(`Elevator index must be between 0 and ${config.elevatorCount - 1}`);
      }

      return state.elevators[elevatorIndex];
    },

    elevatorCallButtonUpPressed: (floorIndex) => {
      if(floorIndex < 0 || floorIndex > state.floors.length-2) {
        throw new Error('Floor Index is invalid: ' + floorIndex);
      }
      // Once lit, the system will have to turn it off:
      state.floors[floorIndex].callButtonUpLit = true;
    },

    elevatorCallButtonDownPressed: (floorIndex) => {
      if(floorIndex < 2 || floorIndex > state.floors.length-1) {
        throw new Error('Floor Index is invalid: ' + floorIndex);
      }
      // Once lit, the system will have to turn it off:
      state.floors[floorIndex].callButtonDownLit = true;
    },

    // Elevator door has opened on given floor:
    elevatorDoorOpened: (elevatorIndex, floorIndex) => {
    },

    // Elevator door has closed on given floor:
    elevatorDoorClosed: (elevatorIndex, floorIndex) => {
    },

    // Update elevator movement:
    elevatorMoved: (elevatorIndex, floorIndex, direction) => {
      // TODO: Also check against array length:
      if (elevatorIndex < 1 || elevatorIndex >= config.elevatorCount) {
        throw new Error(`Elevator index must be between 0 and ${config.elevatorCount - 1}`);
      } else if (floorIndex < 1 || floorIndex >= config.floorCount) {
        throw new Error(`Elevator index must be between 0 and ${config.floorCount - 1}`);
      } else if (direction !== 'up'  && direction !== 'down') {
        throw new Error(`Direction must be up or down, was: "${direction}"`);
      }

      // Update status in memory:
      let usage;
      let elevator = state.elevators[elevatorIndex];
      let currentFloor = elevator.currentFloor;

      if (currentFloor != floorIndex) {
        usage = parseInt(elevator.usage) + Math.abs(currentFloor - floorIndex);
        console.log('Updating the elevator elevatorIndex, currentFloor, floorIndex, usage', elevatorIndex, currentFloor, floorIndex, usage);
        elevator.currentFloor = floorIndex;
        elevator.usage = usage;
        elevator.trips = parseInt(elevator.trips) + 1;

        // TODO: We could assert that supplied direction matches
        // a direction calculated using previous and current floor:
        elevator.direction = direction;
      }
    },
  }
}

module.exports = ElevatorSystem;
